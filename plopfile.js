const component = require("./plop/generators/component"),
  form = require("./plop/generators/form"),
  page = require("./plop/generators/page"),
  middleware = require("./plop/generators/middleware"),
  store = require("./plop/generators/store");

module.exports = plop => {
  plop.setGenerator("component", component);
  plop.setGenerator("form", form);
  plop.setGenerator("page", page);
  plop.setGenerator("middleware", middleware);
  plop.setGenerator("store", store);
};
