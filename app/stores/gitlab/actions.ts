import Axios from "axios";
import { List, Record } from "immutable";
import { state } from "../../lib/state";
import { syncWrap } from "../sync";
import { Gitlab, IGitlab, IProject, Project, STORE_NAME } from "./definitions";

export const baseURL = "https://gitlab.com/api/v4";
export const userId = "nathanfaucett";

export const store = state.getStore(STORE_NAME);

export const ProjectFromJSON = (json: ReturnType<Record<IProject>["toJS"]>) =>
  Project(json);

store.fromJSON = (json: ReturnType<Record<IGitlab>["toJS"]>) =>
  Gitlab({
    projects: List(json.projects.map(ProjectFromJSON))
  });

export const allProjects = (page: number = 1, perPage: number = 8) =>
  syncWrap(
    Axios.get(
      `${baseURL}/users/${userId}/projects?page=${page}&per_page=${perPage}`,
      { withCredentials: false }
    ).then(response => {
      const projects = List<Record<IProject>>(
        response.data.map(ProjectFromJSON)
      );
      store.updateState(state =>
        state
          .set("projects", projects)
          .set("total", +response.headers["x-total"] || 0)
      );
      return projects;
    })
  );
