import { List, Record } from "immutable";

export interface IProject {
  id: number;
  description: string;
  default_branch: string;
  ssh_url_to_repo: string;
  http_url_to_repo: string;
  web_url: string;
  readme_url: string;
  tag_list: string[];
  name: string;
  name_with_namespace: string;
  path: string;
  path_with_namespace: string;
  created_at: Date;
  last_activity_at: Date;
  forks_count: number;
  avatar_url: string;
  star_count: number;
}

export const Project = Record<IProject>({
  id: 0,
  description: "",
  default_branch: "master",
  ssh_url_to_repo: "",
  http_url_to_repo: "",
  web_url: "",
  readme_url: "",
  tag_list: [],
  name: "",
  name_with_namespace: "",
  path: "",
  path_with_namespace: "",
  created_at: new Date(),
  last_activity_at: new Date(),
  forks_count: 0,
  avatar_url: "",
  star_count: 0
});

export interface IGitlab {
  total: number;
  projects: List<Record<IProject>>;
}

export const Gitlab = Record<IGitlab>({
  total: 0,
  projects: List()
});

export const INITIAL_STATE = Gitlab();
export const STORE_NAME = "gitlab";
