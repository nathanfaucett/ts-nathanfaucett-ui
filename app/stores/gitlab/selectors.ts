import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectTotal = (state: IState) =>
  state.get(STORE_NAME).get("total");
export const selectProjects = (state: IState) =>
  state.get(STORE_NAME).get("projects");
