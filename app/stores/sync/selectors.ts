import { IState } from "../../lib/state";
import { STORE_NAME } from "./definitions";

export const isSyncing = (state: IState) => state.get(STORE_NAME).syncing > 0;
