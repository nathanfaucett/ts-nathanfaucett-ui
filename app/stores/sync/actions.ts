import { state } from "../../lib/state";
import { ISyncJSON, STORE_NAME, Sync } from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: ISyncJSON) =>
  Sync({
    syncing: json.syncing
  });

export const start = () =>
  store.updateState(state => state.update("syncing", count => count + 1));
export const end = () =>
  store.updateState(state => state.update("syncing", count => count - 1));

export const syncWrap = <T>(promise: Promise<T>): Promise<T> => {
  start();
  return promise
    .then(result => {
      end();
      return result;
    })
    .catch(e => {
      end();
      throw e;
    });
};
