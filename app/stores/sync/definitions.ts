import { Record } from "immutable";

export interface ISync {
  syncing: number;
}

export const Sync = Record<ISync>({
  syncing: 0
});

export type ISyncJSON = ReturnType<Record<ISync>["toJS"]>;

export const INITIAL_STATE = Sync();
export const STORE_NAME = "sync";
