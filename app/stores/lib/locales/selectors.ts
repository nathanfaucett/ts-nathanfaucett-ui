import { IState } from "../../../lib/state";
import { DEFAULT_LOCALE, STORE_NAME } from "./definitions";

export const selectLocale = (state: IState) =>
  state.get(STORE_NAME).get("locale", DEFAULT_LOCALE);

export const selectMessages = (state: IState) => {
  const messages = state
    .get(STORE_NAME)
    .get("messages")
    .get(state.get(STORE_NAME).get("locale", DEFAULT_LOCALE));

  if (messages) {
    return messages;
  } else {
    return null;
  }
};
