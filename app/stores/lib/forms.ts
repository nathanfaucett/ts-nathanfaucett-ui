import { createFormsStore } from "@stembord/state-forms";
import { Consumer, state } from "../../lib/state";

export const {
  create,
  remove,
  selectForm,
  selectFormExists,
  selectField,
  updateField,
  changeField,
  removeField,
  store,
  injectForm
} = createFormsStore(state, Consumer);
