import { Map } from "immutable";
import { IState } from "../../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectState = (state: IState): string =>
  state.get(STORE_NAME).get("state") || "";
export const selectContext = (state: IState): Map<string, any> =>
  state.get(STORE_NAME).get("context", Map());

export const selectUrlParam = (
  state: IState,
  param: string,
  defaultValue?: string
): string =>
  selectContext(state)
    .get("params", Map())
    .get(param, defaultValue);

export const selectQueryParam = (
  state: IState,
  param: string,
  defaultValue?: number | string
): number | string =>
  selectContext(state)
    .get("url", Map())
    .get("query", Map())
    .get(param, defaultValue);

export const selectParam = (state: IState, param: string): string | number =>
  selectUrlParam(state, param, selectQueryParam(state, param) as any);
