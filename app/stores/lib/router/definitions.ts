import { IContext } from "@stembord/router";
import { Map, Record } from "immutable";
import * as url from "url";

export type IRouterContext = IContext<{
  redirectUrl?: url.UrlWithParsedQuery;
}>;

export interface IRouter {
  state?: string;
  nextUrl?: url.UrlWithParsedQuery;
  url?: url.UrlWithParsedQuery;
  context: Map<string, any>;
}

export const Router = Record<IRouter>({
  state: undefined,
  nextUrl: undefined,
  url: undefined,
  context: Map({})
});

export type IRouterJSON = ReturnType<Record<IRouter>["toJS"]>;

export const STORE_NAME = "router";
export const INITIAL_STATE = Router();
