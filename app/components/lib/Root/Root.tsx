import { default as React } from "react";
import { Locales } from "../Locales";
import { RouteComponent } from "../RouteComponent";
import { StateProvider } from "../StateProvider";

export interface IRootProps {}
export interface IRootState {}

export class Root extends React.PureComponent<IRootProps, IRootState> {
  render() {
    return (
      <StateProvider>
        <Locales>
          <RouteComponent />
        </Locales>
      </StateProvider>
    );
  }
}
