import { default as React } from "react";

const RE_NEWLINE = /[^\r\n]+/g;

export interface IJSErrorProps {
  error: Error;
}
export interface IJSErrorState {}

export class JSError extends React.PureComponent<IJSErrorProps, IJSErrorState> {
  render() {
    const { error } = this.props;

    console.error(error);

    return (
      <div>
        <h6>{error.name}</h6>
        <p>{error.message}</p>
        {(error.stack || "").split(RE_NEWLINE).map((line, index) => (
          <p key={index}>{line}</p>
        ))}
      </div>
    );
  }
}
