import { default as React } from "react";
import { Nav, Navbar } from "reactstrap";
import { connect } from "../../../lib/state";
import { selectHeight, selectWidth } from "../../../stores/lib/screenSize";
import { isSyncing } from "../../../stores/sync";
import { Sizes } from "../Sizes";
import { Brand } from "./Brand";
import { Contact } from "./Contact";
import { Footer } from "./Footer";

interface IHeaderStateProps {
  width: number;
  height: number;
  isSyncing: boolean;
}
interface IHeaderFunctionProps {}

interface IHeaderImplProps extends IHeaderStateProps, IHeaderFunctionProps {}

export interface IHeaderProps {}
export interface IHeaderState {}

const HeaderConnect = connect<
  IHeaderStateProps,
  IHeaderFunctionProps,
  IHeaderProps
>(
  (state, ownProps) => ({
    width: selectWidth(state),
    height: selectHeight(state),
    isSyncing: isSyncing(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class HeaderImpl extends React.PureComponent<IHeaderImplProps, IHeaderState> {
  render() {
    return (
      <Sizes
        refs={["header"]}
        width={this.props.width}
        height={this.props.height}
      >
        {(sizes, refs) => (
          <>
            <div ref={refs.header as React.RefObject<HTMLDivElement>}>
              <Navbar color="white" light className="border-bottom">
                <Brand />
                <Nav className="mr-auto" />
                {this.props.isSyncing && (
                  <div
                    className="spinner-border text-secondary mr-2"
                    role="status"
                  >
                    <span className="sr-only">Loading...</span>
                  </div>
                )}
                <Contact />
              </Navbar>
            </div>
            <div
              style={{
                overflowY: "scroll",
                height: this.props.height - sizes.header.height
              }}
            >
              {this.props.children}
              <Footer />
            </div>
          </>
        )}
      </Sizes>
    );
  }
}

export const Header = HeaderConnect(HeaderImpl);
