import * as React from "react";
import { injectIntl, IntlShape } from "react-intl";
import { Form, Input } from "reactstrap";
import { connect } from "../../../../lib/state";
import {
  ISupporttedLocale,
  selectLocale,
  setLocale,
  supportLocales
} from "../../../../stores/lib/locales";

interface ILocaleSelectStateProps {
  fullWidth?: boolean;
  locale: string;
  locales: ISupporttedLocale[];
}
interface ILocaleSelectFunctionProps {}
interface ILocaleSelectImplProps
  extends ILocaleSelectStateProps,
    ILocaleSelectFunctionProps {
  intl: IntlShape;
}

export interface ILocaleSelectProps {
  fullWidth?: boolean;
}
export interface ILocaleSelectState {}

const LocaleSelectConnect = connect<
  ILocaleSelectStateProps,
  ILocaleSelectFunctionProps,
  ILocaleSelectProps
>(
  (state, ownProps) => ({
    locale: selectLocale(state),
    locales: supportLocales()
  }),
  (state, ownProps, stateProps) => ({})
);

class LocaleSelectImpl extends React.PureComponent<
  ILocaleSelectImplProps,
  ILocaleSelectState
> {
  onChange = (e: any) => {
    setLocale(e.target.value);
  };
  render() {
    return (
      <Form inline>
        <Input
          bsSize="sm"
          onChange={this.onChange}
          type="select"
          value={this.props.locale}
        >
          {this.props.locales.map(locale => (
            <option key={locale} value={locale}>
              {this.props.intl.formatMessage({ id: `app.locales.${locale}` })}
            </option>
          ))}
        </Input>
      </Form>
    );
  }
}

export const LocaleSelect = LocaleSelectConnect(injectIntl(LocaleSelectImpl));
