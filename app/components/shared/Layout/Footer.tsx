import { default as React } from "react";
import { FormattedMessage } from "react-intl";

export interface IHeaderProps {
  refObject?: React.RefObject<HTMLDivElement>;
}
export interface IHeaderState {}

export class Footer extends React.PureComponent<IHeaderProps, IHeaderState> {
  render() {
    return (
      <div
        ref={this.props.refObject}
        className="pt-4 pb-4 page-footer border-top"
      >
        <p className="m-0 text-center">
          <FormattedMessage
            id="app.footer.copyright"
            values={{
              year: new Date().getFullYear(),
              value: <a href="/">nathanfaucett.com</a>
            }}
          />
        </p>
      </div>
    );
  }
}
