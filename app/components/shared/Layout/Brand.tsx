import { default as React } from "react";
import { FormattedMessage } from "react-intl";
import { NavbarBrand } from "reactstrap";

export interface IBrandProps {}
export interface IBrandState {}

export class Brand extends React.PureComponent<IBrandProps, IBrandState> {
  constructor(props: IBrandProps) {
    super(props);
  }
  render() {
    return (
      <NavbarBrand href="/">
        <FormattedMessage id="app.name" />
      </NavbarBrand>
    );
  }
}
