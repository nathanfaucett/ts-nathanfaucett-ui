import { default as React } from "react";

export interface IContactProps {}
export interface IContactState {}

export class Contact extends React.PureComponent<IContactProps, IContactState> {
  constructor(props: IContactProps) {
    super(props);
  }
  render() {
    return (
      <>
        <a href="mailto:nathanfaucett@gmail.com" target="_blank">
          nathanfaucett@gmail.com
        </a>
        &nbsp;
        <a href="tel:1-256-525-6999" target="_blank">
          1-256-525-6999
        </a>
      </>
    );
  }
}
