import { IInputProps } from "@stembord/state-forms";
import { default as React } from "react";
import { FormGroup, Input, Label } from "reactstrap";
import { FormFieldErrorMessage } from "../FormFieldErrorMessage";

export type ITextFieldProps = IInputProps<string> & {
  type?: string;
  placeholder?: string;
  disabled?: boolean;
  label?: React.ReactChild;
};
export interface ITextFieldState {}

export class TextField extends React.PureComponent<
  ITextFieldProps,
  ITextFieldState
> {
  render() {
    const {
      error,
      errors,
      focus,
      onChange,
      change,
      label,
      type,
      ...props
    } = this.props;

    return (
      <FormGroup>
        {label && <Label>{this.props.label}</Label>}
        <Input
          {...props}
          type={type as "text"}
          onChange={onChange as any}
          invalid={error}
        />
        <FormFieldErrorMessage errors={errors} />
      </FormGroup>
    );
  }
}
