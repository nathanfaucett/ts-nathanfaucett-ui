import { IChangesetError } from "@stembord/changeset";
import { List, Record } from "immutable";
import { default as React } from "react";
import { InjectedIntl, injectIntl } from "react-intl";
import { FormFeedback } from "reactstrap";

export interface IFormFieldErrorMessageProps {
  intl: InjectedIntl;
  errors: List<Record<IChangesetError>>;
  translationScope?: string;
}
export interface IFormFieldErrorMessageState {}

class FormFieldErrorMessageImpl extends React.PureComponent<
  IFormFieldErrorMessageProps,
  IFormFieldErrorMessageState
> {
  static defaultProps = {
    translationScope: "error"
  };

  render() {
    return this.props.errors.map((error, index) => {
      const message = this.props.intl.formatMessage(
        { id: `${this.props.translationScope}.${error.get("message")}` },
        { value: error.get("values").join(" ") }
      );

      return (
        <FormFeedback type="invalid" key={index}>
          {message}
        </FormFeedback>
      );
    });
  }
}

export const FormFieldErrorMessage = injectIntl(FormFieldErrorMessageImpl);
