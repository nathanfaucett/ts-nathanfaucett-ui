import { default as React } from "react";
import {
  Pagination as BSPagination,
  PaginationItem,
  PaginationLink,
  PaginationProps as BSPaginationProps
} from "reactstrap";

export interface IPaginationProps extends BSPaginationProps {
  refObject: React.RefObject<Element>;
  maxWidth: number;
  totalItems: number;
  pageSize: number;
  maxPaginationNumbers?: number;
  activePage?: number;
  firstPageText?: string;
  previousPageText?: string;
  nextPageText?: string;
  lastPageText?: string;
  onChangePage(activePage: number): void;
}
export interface IPaginationState {
  activePage: number;
  firstPaginationNumber: number;
}

export class Pagination extends React.PureComponent<
  IPaginationProps,
  IPaginationState
> {
  static defaultProps = {
    maxPaginationNumbers: 5,
    activePage: 1,
    firstPageText: "First",
    previousPageText: "Previous",
    nextPageText: "Next",
    lastPageText: "Last"
  };
  private pages: number = 0;
  private lastPaginationNumber: number = 0;

  constructor(props: IPaginationProps) {
    super(props);

    this.state = {
      activePage: this.props.activePage as number,
      firstPaginationNumber: 1
    };
    this.pages = this.getNumberOfPages(this.props);
  }
  componentWillReceiveProps(props: IPaginationProps) {
    this.pages = this.getNumberOfPages(props);
    this.setState({
      activePage: props.activePage as number
    });
    this.forceUpdate();
  }

  getNumberOfPages = (props: IPaginationProps) => {
    const auxPages = props.totalItems / props.pageSize;
    let pages = auxPages | 0;
    pages += pages !== auxPages ? 1 : 0;
    return pages;
  };

  paginationItems = () => {
    const items = [];

    this.lastPaginationNumber = this.getLastPaginationNumber();

    if (this.props.maxWidth >= 320) {
      items.push(this.firstOrLastPagItem(this.props.firstPageText || "", 1));
    }
    items.push(
      this.nextOrPreviousPagItem(this.props.previousPageText || "", 1, "l")
    );

    if (this.props.maxWidth >= 480) {
      for (
        let i = this.state.firstPaginationNumber;
        i <= this.lastPaginationNumber;
        i++
      ) {
        items.push(this.numberedPagItem(i));
      }
    }

    items.push(
      this.nextOrPreviousPagItem(this.props.nextPageText || "", this.pages, "r")
    );
    if (this.props.maxWidth >= 320) {
      items.push(
        this.firstOrLastPagItem(this.props.lastPageText || "", this.pages)
      );
    }

    return items;
  };

  getLastPaginationNumber = () => {
    const minNumberPages = Math.min(this.pages, this.props
      .maxPaginationNumbers as number);

    return this.state.firstPaginationNumber + minNumberPages - 1;
  };

  numberedPagItem = (i: number) => {
    return (
      <PaginationItem
        key={i}
        id={`pagebutton${i}`}
        active={this.state.activePage === i}
        onClick={this.handleClick}
      >
        <PaginationLink style={{ minWidth: "43.5px" }}>{i}</PaginationLink>
      </PaginationItem>
    );
  };

  nextOrPreviousPagItem = (
    name: React.Key,
    page: number,
    direction: string
  ) => {
    return (
      <PaginationItem
        key={name}
        disabled={this.state.activePage === page}
        onClick={e => this.handleSelectNextOrPrevious(direction)}
      >
        <PaginationLink>{name}</PaginationLink>
      </PaginationItem>
    );
  };

  firstOrLastPagItem = (name: React.Key, page: number) => {
    const event = {
      currentTarget: {
        getAttribute: () => `pagebutton${page}`
      }
    };

    return (
      <PaginationItem
        key={name}
        disabled={this.state.activePage === page}
        onClick={() => this.handleClick(event as any)}
      >
        <PaginationLink>{name}</PaginationLink>
      </PaginationItem>
    );
  };

  handleClick = (event: React.MouseEvent<any>) => {
    const newActivePage = parseInt(
      event.currentTarget
        .getAttribute("id")
        .split("pagebutton")
        .pop(),
      10
    );

    this.setState({
      activePage: newActivePage
    });
    this.handlePaginationNumber(newActivePage);
    this.props.onChangePage(newActivePage);
  };

  handleSelectNextOrPrevious = (direction: string) => {
    const activePage = this.state.activePage;

    if (
      (direction === "r" && activePage === this.pages) ||
      (direction === "l" && activePage === 1)
    ) {
      return;
    }

    const newActivePage = direction === "r" ? activePage + 1 : activePage - 1;

    this.setState({
      activePage: newActivePage
    });

    this.handlePaginationNumber(newActivePage);
    this.props.onChangePage(newActivePage);
  };

  handlePaginationNumber = (activePage: number) => {
    const distance = Math.floor(
      (this.props.maxPaginationNumbers as number) / 2
    );
    const newFPNumber = activePage - distance;
    const newLPNumber = activePage + distance;

    if (newFPNumber <= distance) {
      if (this.state.firstPaginationNumber !== 1) {
        this.setState({
          firstPaginationNumber: 1
        });
      }
    } else if (newLPNumber <= this.pages) {
      this.setState({
        firstPaginationNumber: newFPNumber
      });
    } else if (newLPNumber >= this.pages) {
      this.setState({
        firstPaginationNumber:
          this.pages - (this.props.maxPaginationNumbers as number) + 1
      });
    }
  };

  render() {
    const {
      maxWidth,
      totalItems,
      pageSize,
      maxPaginationNumbers,
      activePage,
      firstPageText,
      previousPageText,
      nextPageText,
      lastPageText,
      onChangePage,
      ...props
    } = this.props;

    return (
      <div ref={this.props.refObject as React.RefObject<HTMLDivElement>}>
        <BSPagination {...props}>{this.paginationItems()}</BSPagination>
      </div>
    );
  }
}
