import { List, Record } from "immutable";
import * as React from "react";
import { Card, CardBody, CardImg, Col, Row } from "reactstrap";
import { allProjects, IProject } from "../../../stores/gitlab";
import { Pagination } from "../../shared/Pagination";
import { Sizes } from "../../shared/Sizes";

export interface IProjectsProps {
  height: number;
  width: number;
  total: number;
  projects: List<Record<IProject>>;
}
export interface IProjectsState {
  page: number;
  rowsPerPage: number;
}

export class Projects extends React.PureComponent<
  IProjectsProps,
  IProjectsState
> {
  constructor(props: IProjectsProps) {
    super(props);

    this.state = {
      page: 1,
      rowsPerPage: 8
    };
  }
  getAllProject = () => {
    allProjects(this.state.page, this.state.rowsPerPage);
  };
  onChangePage = (page: number) => {
    this.setState(
      {
        page
      },
      this.getAllProject
    );
  };
  componentDidMount() {
    this.getAllProject();
  }
  render() {
    return (
      <>
        <Row>
          {this.props.projects.map(project => (
            <Col key={project.get("id")} sm={12} md={6} lg={4} className="mb-4">
              <Card>
                <CardImg src={project.get("avatar_url")} />
                <CardBody>
                  <h3>
                    <a
                      className="mr-2"
                      target="_blank"
                      href={project.get("http_url_to_repo")}
                    >
                      {project.get("name")}
                    </a>
                  </h3>
                  <p>{project.get("description")}</p>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
        <div className="text-center mb-4">
          <Sizes
            refs={["pagination"]}
            width={this.props.width}
            height={this.props.height}
          >
            {(sizes, refs) => (
              <Pagination
                refObject={refs.pagination}
                maxWidth={sizes.pagination.width}
                className="d-inline-block"
                totalItems={this.props.total}
                pageSize={this.state.rowsPerPage}
                activePage={this.state.page}
                onChangePage={this.onChangePage}
              />
            )}
          </Sizes>
        </div>
      </>
    );
  }
}
