import { List, Record } from "immutable";
import * as React from "react";
import { FormattedMessage } from "react-intl";
import {
  Card,
  CardBody,
  CardSubtitle,
  CardText,
  CardTitle,
  Container
} from "reactstrap";
import { connect } from "../../../lib/state";
import { selectWidth, selectHeight } from "../../../stores/lib/screenSize";
import { IProject, selectProjects, selectTotal } from "../../../stores/gitlab";
import { Layout } from "../../shared/Layout";
import { Projects } from "./Projects";

interface IIndexStateProps {
  width: number;
  height: number;
  total: number;
  projects: List<Record<IProject>>;
}
interface IIndexFunctionProps {}
interface IIndexImplProps extends IIndexStateProps, IIndexFunctionProps {}

export interface IIndexProps {}
export interface IIndexState {}

const IndexConnect = connect<
  IIndexStateProps,
  IIndexFunctionProps,
  IIndexProps
>(
  (state, ownProps) => ({
    width: selectWidth(state),
    height: selectHeight(state),
    total: selectTotal(state),
    projects: selectProjects(state)
  }),
  (state, ownProps, stateProps) => ({})
);

class IndexImpl extends React.PureComponent<IIndexImplProps, IIndexState> {
  render() {
    return (
      <Layout>
        <Container>
          <h1 className="mt-4">
            <FormattedMessage id="resume.about.header" />
          </h1>
          <Card>
            <CardBody>
              <p>
                <FormattedMessage
                  id="resume.about.body1"
                  values={{
                    libraries: (
                      <a
                        href="https://gitlab.com/stembord/libs"
                        target="_blank"
                      >
                        <FormattedMessage id="resume.about.libraries" />
                      </a>
                    ),
                    ui: (
                      <a
                        href="https://gitlab.com/nathanfaucett/ts-nathanfaucett-ui"
                        target="_blank"
                      >
                        <FormattedMessage id="resume.about.ui" />
                      </a>
                    ),
                    server: (
                      <a
                        href="https://gitlab.com/stembord/ops/ops-server"
                        target="_blank"
                      >
                        <FormattedMessage id="resume.about.server" />
                      </a>
                    )
                  }}
                />
              </p>
              <p>
                <FormattedMessage
                  id="resume.about.body2"
                  values={{
                    react: (
                      <a href="https://reactjs.org/" target="_blank">
                        <FormattedMessage id="resume.about.react" />
                      </a>
                    ),
                    state: (
                      <a
                        href="https://gitlab.com/stembord/libs/ts-state"
                        target="_blank"
                      >
                        <FormattedMessage id="resume.about.state" />
                      </a>
                    ),
                    stateReact: (
                      <a
                        href="https://gitlab.com/stembord/libs/ts-state-react"
                        target="_blank"
                      >
                        <FormattedMessage id="resume.about.state_react" />
                      </a>
                    ),
                    jenkins: (
                      <a href="https://jenkins.io/" target="_blank">
                        <FormattedMessage id="resume.about.jenkins" />
                      </a>
                    ),
                    helm: (
                      <a href="https://helm.sh/" target="_blank">
                        <FormattedMessage id="resume.about.helm" />
                      </a>
                    ),
                    kubernetes: (
                      <a href="https://kubernetes.io/" target="_blank">
                        <FormattedMessage id="resume.about.kubernetes" />
                      </a>
                    ),
                    letsencrypt: (
                      <a href="https://letsencrypt.org/" target="_blank">
                        <FormattedMessage id="resume.about.letsencrypt" />
                      </a>
                    )
                  }}
                />
              </p>
            </CardBody>
          </Card>
          <h1 className="mt-4">
            <FormattedMessage id="resume.skills.header" />
          </h1>
          <Card>
            <CardBody>
              <CardText>
                <FormattedMessage id="resume.skills.body" />
              </CardText>
            </CardBody>
          </Card>
          <h1 className="mt-4">
            <FormattedMessage id="resume.experience.header" />
          </h1>
          <Card className="mb-3">
            <CardBody>
              <CardTitle>
                <h3>
                  <FormattedMessage id="resume.experience.ignitionone.title" />
                </h3>
              </CardTitle>
              <hr />
              <CardSubtitle>
                <h6>
                  <FormattedMessage id="resume.experience.ignitionone.tagline" />
                </h6>
                <h6>
                  <FormattedMessage id="resume.experience.ignitionone.dates" />
                </h6>
              </CardSubtitle>
              <hr />
              <ul>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.ignitionone.body0" />
                  </p>
                </li>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.ignitionone.body1" />
                  </p>
                </li>
              </ul>
            </CardBody>
          </Card>
          <Card className="mb-3">
            <CardBody>
              <CardTitle>
                <h3>
                  <FormattedMessage id="resume.experience.testcloud.title" />
                </h3>
              </CardTitle>
              <hr />
              <CardSubtitle>
                <h6>
                  <FormattedMessage id="resume.experience.testcloud.tagline" />
                </h6>
                <h6>
                  <FormattedMessage id="resume.experience.testcloud.dates" />
                </h6>
              </CardSubtitle>
              <hr />
              <ul>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.testcloud.body0" />
                  </p>
                </li>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.testcloud.body1" />
                  </p>
                </li>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.testcloud.body2" />
                  </p>
                </li>
              </ul>
            </CardBody>
          </Card>
          <Card className="mb-3">
            <CardBody>
              <CardTitle>
                <h3>
                  <FormattedMessage id="resume.experience.thinkit_design.title" />
                </h3>
              </CardTitle>
              <hr />
              <CardSubtitle>
                <h6>
                  <FormattedMessage id="resume.experience.thinkit_design.tagline" />
                </h6>
                <h6>
                  <FormattedMessage id="resume.experience.thinkit_design.dates" />
                </h6>
              </CardSubtitle>
              <hr />
              <ul>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.thinkit_design.body0" />
                  </p>
                </li>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.thinkit_design.body1" />
                  </p>
                </li>
                <li>
                  <p>
                    <FormattedMessage id="resume.experience.thinkit_design.body2" />
                  </p>
                </li>
              </ul>
            </CardBody>
          </Card>
          <h1 className="mt-4">
            <FormattedMessage id="resume.education.header" />
          </h1>
          <Card>
            <CardBody>
              <CardTitle>
                <h3>
                  <FormattedMessage id="resume.education.jsu.title" />
                </h3>
              </CardTitle>
              <hr />
              <CardSubtitle>
                <h6>
                  <FormattedMessage id="resume.education.jsu.tagline" />
                </h6>
                <h6>
                  <FormattedMessage id="resume.education.jsu.dates" />
                </h6>
              </CardSubtitle>
              <hr />
              <ul>
                <li>
                  <p>
                    <FormattedMessage id="resume.education.jsu.body0" />
                  </p>
                </li>
              </ul>
            </CardBody>
          </Card>
          <h1 className="mt-4 mb-4">
            <FormattedMessage id="resume.projects.header" />
          </h1>
          <Projects width={this.props.width} height={this.props.height} projects={this.props.projects} total={this.props.total} />
        </Container>
      </Layout>
    );
  }
}

export const Index = IndexConnect(IndexImpl);
