import * as React from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { Page } from "../../lib/Page";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const IndexPage = injectIntl(({ intl }) => (
  <Async
    promise={import("./Index")}
    onSuccess={({ Index }) => (
      <Page>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.home" })}</title>
        </Helmet>
        <Index />
      </Page>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
