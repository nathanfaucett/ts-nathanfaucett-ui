import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";
import { Page } from "../../lib/Page";

export const Error404Page = injectIntl(({ intl }) => (
  <Async
    promise={import("./Error404")}
    onSuccess={({ Error404 }) => (
      <Page>
        <div>
          <Helmet>
            <title>{intl.formatMessage({ id: "app.page.error_404" })}</title>
          </Helmet>
          <Error404 />
        </div>
      </Page>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
