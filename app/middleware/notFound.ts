import { IHandler } from "@stembord/router";
import { parse } from "url";
import { IRouterContext } from "../stores/lib/router";

export const notFound: IHandler = (context: IRouterContext) => {
  context.redirectUrl = parse("/404", true);
};
