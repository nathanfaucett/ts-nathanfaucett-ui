import { Error404Page } from "./components/pages/Error404/Error404Page";
import { IndexPage } from "./components/pages/Index/IndexPage";
import { renderApp } from "./lib/internal";
import { router } from "./lib/router";
import { notFound } from "./middleware/notFound";

const init = () => {
  router.page("Error404", "/404", Error404Page);

  /* DO NOT REMOVE THIS */
  router.page("Index", "/", IndexPage);

  router.use("/", notFound);
};

init();

if (process.env.NODE_ENV !== "production") {
  if ((module as any).hot) {
    ((module as any).hot as any).accept(() => {
      router.clear();
      init();
      renderApp();
    });
  }
}