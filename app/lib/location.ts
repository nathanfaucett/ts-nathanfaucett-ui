import { IHandler, Location } from "@stembord/location";
import { createContext } from "@stembord/router";
import { parse, UrlWithParsedQuery } from "url";
import { set } from "../stores/lib/router";
import { router } from "./router";

const handler: IHandler = (url: UrlWithParsedQuery) => {
  const context = createContext(url);

  set(context);

  return router.handle(context).then(
    () => {
      if (context.redirectUrl) {
        return Promise.reject(context.redirectUrl);
      } else if (!context.resolved) {
        return Promise.reject(parse("/404"));
      } else {
        return Promise.resolve(context.url);
      }
    },
    error => {
      if (error && context.redirectUrl) {
        return Promise.reject(context.redirectUrl);
      } else if (error) {
        console.error(error);
        return Promise.reject(error);
      } else if (!context.resolved) {
        return Promise.reject(parse("/404"));
      } else {
        return Promise.reject(null);
      }
    }
  );
};

export const location = new Location(window, { html5Mode: true, handler });
