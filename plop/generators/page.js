const path = require("path"),
  { RE_START_ENTRY, RE_ROUTES_ENTRY } = require("../utils");

module.exports = {
  description: "Page Component",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "component name"
    },
    {
      type: "input",
      name: "path",
      message: "path for routes"
    }
  ],
  actions: data => [
    {
      type: "add",
      path: `app/components/pages/{{name}}/{{name}}.tsx`,
      data: {
        backPath: path.relative(`./app/components/pages/${data.name}`, `./app`)
      },
      templateFile: "plop/templates/Component/StateComponent.tsx.hbs"
    },
    {
      type: "add",
      path: `app/components/pages/{{name}}/index.ts`,
      templateFile: "plop/templates/Component/export_Component.ts.hbs"
    },
    {
      type: "add",
      path: "app/components/pages/{{name}}/{{name}}Page.tsx",
      templateFile: "plop/templates/Component/Page.tsx.hbs"
    },
    {
      type: "modify",
      path: "app/routes.ts",
      pattern: RE_START_ENTRY,
      template:
        'import { {{name}}Page } from "./components/pages/{{name}}/{{name}}Page";\n'
    },
    {
      type: "append",
      path: "app/routes.ts",
      pattern: RE_ROUTES_ENTRY,
      template: '\trouter.page("{{name}}", "{{path}}", {{name}}Page);'
    }
  ]
};
